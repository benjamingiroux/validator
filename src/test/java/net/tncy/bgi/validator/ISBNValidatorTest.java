package net.tncy.bgi.validator;

import jakarta.validation.ClockProvider;
import jakarta.validation.ConstraintValidatorContext;
import org.junit.Test;
import net.tncy.bgi.validator.ISBNValidator;

import static junit.framework.TestCase.assertTrue;

public class ISBNValidatorTest {

    @Test
    public void validatorShouldReturnTrue() {
        assertTrue(new ISBNValidator().isValid("1", new ConstraintValidatorContext() {
            @Override
            public void disableDefaultConstraintViolation() {

            }

            @Override
            public String getDefaultConstraintMessageTemplate() {
                return null;
            }

            @Override
            public ClockProvider getClockProvider() {
                return null;
            }

            @Override
            public ConstraintViolationBuilder buildConstraintViolationWithTemplate(String s) {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> aClass) {
                return null;
            }
        }));
    }
}
